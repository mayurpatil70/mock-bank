package com.mockbank.converters;

import com.mockbank.dto.AccountDTO;
import com.mockbank.dto.AccountResponseDTO;
import com.mockbank.dto.AddressDTO;
import com.mockbank.entity.Account;
import com.mockbank.entity.Address;

public class AccountDtoConverter {

    public static AccountResponseDTO toDTO(Account account) {
        AccountResponseDTO dto = new AccountResponseDTO();
        dto.setFirstName(account.getFirstName());
        dto.setLastName(account.getLastName());
        dto.setAccountNo(account.getAccountNumber());
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setDoorNumber(account.getAddress().getDoorNumber());
        addressDTO.setStreetName(account.getAddress().getStreetName());
        addressDTO.setCity(account.getAddress().getCity());
        addressDTO.setDistrict(account.getAddress().getDistrict());
        addressDTO.setState(account.getAddress().getState());
        addressDTO.setCountry(account.getAddress().getCountry());
        addressDTO.setPinCode(account.getAddress().getPinCode());
        dto.setAddressDTO(addressDTO);
        return dto;
    }

    public static Account toAccount(Account account, AccountDTO accountDTO) {
        Address address = new Address();
        address.setDoorNumber(accountDTO.getAddressDTO().getDoorNumber());
        address.setStreetName(accountDTO.getAddressDTO().getStreetName());
        address.setCity(accountDTO.getAddressDTO().getCity());
        address.setDistrict(accountDTO.getAddressDTO().getDistrict());
        address.setState(accountDTO.getAddressDTO().getState());
        address.setCountry(accountDTO.getAddressDTO().getCountry());
        address.setPinCode(accountDTO.getAddressDTO().getPinCode());
        account.setFirstName(accountDTO.getFirstName());
        account.setLastName(accountDTO.getLastName());
        account.setAddress(address);

        return account;
    }

    public static Account toAccount(AccountDTO accountDTO) {
        Account account = new Account();
        Address address = new Address();
        address.setDoorNumber(accountDTO.getAddressDTO().getDoorNumber());
        address.setStreetName(accountDTO.getAddressDTO().getStreetName());
        address.setCity(accountDTO.getAddressDTO().getCity());
        address.setDistrict(accountDTO.getAddressDTO().getDistrict());
        address.setState(accountDTO.getAddressDTO().getState());
        address.setCountry(accountDTO.getAddressDTO().getCountry());
        address.setPinCode(accountDTO.getAddressDTO().getPinCode());
        account.setFirstName(accountDTO.getFirstName());
        account.setLastName(accountDTO.getLastName());
        account.setAddress(address);

        return account;
    }


}