package com.mockbank.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.AfterThrowing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@org.aspectj.lang.annotation.Aspect
@Component
public class Aspect {
    Logger log = LoggerFactory.getLogger("Aspect.class");

    @Pointcut("@annotation(com.mockbank.annotation.CustomLogger)")
    public void pointCut() {
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Request Body : " + new ObjectMapper().writeValueAsString(joinPoint.getArgs()));
        Object object = joinPoint.proceed();
        log.info("Response Body : " + new ObjectMapper().writeValueAsString(object));
        return object;
    }

    @AfterThrowing(value = "@annotation(com.mockbank.annotation.CustomLogger)", throwing = "e")
    public void after(JoinPoint joinPoint, Exception e) {
        log.error("Exception : " + e);
    }

}
